var hwSlideSpeed = 700;
var hwTimeOut = 3000;
var hwNeedLinks = true;
 
$(document).ready(function() {

    $('.slide').css({
        "position" : "absolute",
        "top":'0',
        "left": '0'
    }).hide().eq(0).show();
    var slideNum = 0;
    var slideTime;
    slideCount = $(".gl-slider .slide").size();
    var animSlide = function(arrow) {
        clearTimeout(slideTime);
        $('.slide').eq(slideNum)./*hide('slide', {'direction': 'right'}, hwSlideSpeed, rotator)*/fadeOut(hwSlideSpeed);
        if(arrow == "next") {
            if(slideNum == (slideCount-1)) {slideNum=0;}
            else {slideNum++}
            }
        else if(arrow == "prew")
        {
            if(slideNum == 0) {slideNum=slideCount-1;}
            else {slideNum-=1}
        }
        else{
            slideNum = arrow;
        }
        $('.slide').eq(slideNum)./*show('slide', {'direction': 'left'}, hwSlideSpeed, rotator)*/fadeIn(hwSlideSpeed, rotator);
    }
    if(hwNeedLinks) {     
        $('.nextbutton').click(function() {
            animSlide("next");
            return false;
        });
        $('.prewbutton').click(function() {
            animSlide("prew");
            return false;
        });
    }
    var pause = false;
    var rotator = function() {
        if(!pause) {
            slideTime = setTimeout(function() {
                animSlide('next')
            }, hwTimeOut);
        }
    }
    $('.gl-slider').hover(function() { 
        clearTimeout(slideTime);
        pause = true;
    }, function() {
        pause = false;
        rotator();
    });
    rotator();

    $('.trns-slider').owlCarousel({
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsTabletSmall: false,
        itemsMobile: false,
        autoPlay: true,
        slideSpeed: 300,
        paginationSpeed: 300,
        navigation: true,
        pagination: false,
        theme: false,
        addClassActive: true,
    });
    $('.review-slider').owlCarousel({
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsTabletSmall: false,
        itemsMobile: false,
        autoPlay: true,
        slideSpeed: 500,
        paginationSpeed: 500,
        navigation: true,
        pagination: false,
        theme: false,
        addClassActive: true,
    });

    $('.serv-icon').hover(function() {
       var src =  $(this).find('img').attr('src');
       src = src.substring(0, src.length - 4) + '-active.png';
       $(this).find('img').attr('src', src);
    }, function() {
       var src =  $(this).find('img').attr('src');
       src = src.substring(0, src.length - 11) + '.png';
       $(this).find('img').attr('src', src);
    });

    $('.trns-link').hover(function() {
        $(this).append('<div class="hover"></div>');
        $(this).children('span').css('visibility', 'visible');
    }, function() {
        $(this).children('.hover').remove();
        $(this).children('span').css('visibility', 'hidden');
    });
});